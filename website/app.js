function time() {


    let currentTime = new Date();
    let hour = currentTime.getHours();
    let min = currentTime.getMinutes();
    let sec = currentTime.getSeconds();
    let period = "am";
    //if statement om tijdstip aan te geven per sectie.//
    if ( hour >= 12 ) {
        period = "pm"
    }
    if ( hour > 12) {
        hour = hour - 12
    }
    if ( min < 10) {
        min = "0" + min
    }
    if ( sec <10) {
        sec = "0" + sec
    }

    document.getElementById("current_time").innerHTML = "de tijd is: " + hour + ":" + min + ":" + sec +  " " + period
}

setInterval (time,1000);


